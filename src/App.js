import React, {Component} from 'react';
import './App.css';
import LotteryTicket from "./Components/LotteryTicket/LotteryTicket";

class App extends Component {
    state = {
        numbers: []
    };
    randomNumber = () => Math.floor(Math.random() * (37 - 5) + 5);

    newNumbers = () => {
        let numbers = [];
        for (let i = 1; i <= 5;) {
            const number = this.randomNumber();
            if (numbers.includes(number)) {
                continue;
            }
            numbers.push(number);
            ++i;
        }
        numbers.sort(function(a, b){return a-b;});
        this.setState({numbers});
    };


    render() {
        return (
            <div className="App">
                <button onClick={this.newNumbers}>New numbers</button>
                <br/>
                <div id="ticketsBlock">
                    {this.state.numbers.map((num, id) => (
                        <LotteryTicket key={id} number={num}/>
                    ))}
                </div>
            </div>
        );
    }
}

export default App;
