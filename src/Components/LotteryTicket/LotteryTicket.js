import React from 'react';
import './LotteryTicket.css';

const LotteryTicket = (props)=>(
    <div className="ticket">
        <p>{props.number}</p>
    </div>
);

export default LotteryTicket